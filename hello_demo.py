from bottle import route, run, request, response, static_file, redirect
import json
import pickle
import sys
import os

user_list = 0

sys.path.append(os.path.join(sys.path[0], 'func_saul/modules'))

from classes import Particle, Card, Board
state = 1
question_id = -1
answered = False
answer = ""
the_end = -1
# load datasets
with open('func_saul/dataset/particles.p', 'r') as fd:
    particles = pickle.load(fd)
with open('func_saul/dataset/questions.p', 'r') as fd:
    questions = pickle.load(fd)
with open('func_saul/dataset/explanations.p', 'r') as fd:
    explanations = pickle.load(fd)

board_number = 0
board1 = Board(particles, questions, explanations, shuff=True)
board1.setMyGoal(2)
board2 = Board(particles, questions, explanations, shuff=True)
board2.setMyGoal(12)

wait1, wait2 = True, True

def nextBoard():
    global board_number, board
    res = board_number
    board_number = (board_number + 1) % 2
    return board[res]

##############################################################################################
# FUNCTION: INDEX
# METHOD: GET
# DESCRIPTION:  This method loads the INDEX and welcome webpage
##############################################################################################
@route('/')
@route('/index')
def setting():
    return static_file('index.html', root='html/')

##############################################################################################
# FUNCTION: PLAYER 1 BOARD
# METHOD: GET
# DESCRIPTION:  This method loads the Player 1 board
##############################################################################################
@route('/board1')
def setting():
    return static_file('player1board.html', root='html/')

##############################################################################################
# FUNCTION: PLAYER 2 BOARD
# METHOD: GET
# DESCRIPTION:  This method loads the Player 2 board
##############################################################################################
@route('/board2')
def setting():
    return static_file('player2board.html', root='html/')
##############################################################################################
# FUNCTION: SETTINGS
# METHOD: GET
# DESCRIPTION:  This method loads the settings webpage
##############################################################################################
@route('/settings')
def setting():
    return static_file('settings.html', root='html/')

##############################################################################################
# FUNCTION: CHARTS
# METHOD: GET
# DESCRIPTION:  This method loads the charts webpage.
##############################################################################################
@route('/charts')
def charts():
    return static_file('charts.html', root='html/')

##############################################################################################
# FUNCTION: TABLES
# METHOD: GET
# DESCRIPTION:  This method loads the tables webpage.
##############################################################################################
@route('/tables')
def tables():
    return static_file('tables.html', root='html/')

##############################################################################################
# FUNCTION: CSS FILES
# METHOD: GET
# DESCRIPTION:  This method returns CSS files on demand
##############################################################################################
@route('/static/css/<filename>')
def send_static(filename):
    return static_file(filename, root='static/css')

##############################################################################################
# FUNCTION: IMG FILES
# METHOD: GET
# DESCRIPTION:  This method returns IMg files on demand
##############################################################################################
@route('/static/img/<filename>')
def send_static(filename):
    return static_file(filename, root='static/img')
##############################################################################################
# FUNCTION: JS FILES
# METHOD: GET
# DESCRIPTION:  This method returns JS files on demand
##############################################################################################
@route('/static/js/<filename>')
def send_static(filename):
    return static_file(filename, root='static/js')

##############################################################################################
# FUNCTION: CSS FONT FILES
# METHOD: GET
# DESCRIPTION:  This method returns CSS FONT files on demand
##############################################################################################
@route('/static/css/fonts/<filename>')
def send_static(filename):
    return static_file(filename, root='static/css/fonts')

##############################################################################################
# FUNCTION: CSS FONT FILES
# METHOD: GET
# DESCRIPTION:  This method returns CSS FONT files on demand
##############################################################################################
@route('/random_board', method='POST')
def send_static():
    player = request.forms.get('player')
    if player == '1':
        print "board1"
        return json.dumps(board1.getCardsIDs())
    print "board2"
    return json.dumps(board2.getCardsIDs())

@route('/questions', method='POST')
def send_static():
    player = request.forms.get('player')
    print("player", player)
    if player == '1':
        print "board1"
        return json.dumps(board1.getQuestions())
    print "board2"
    return json.dumps(board2.getQuestions())

@route('/player_particle', method='POST')
def player_particle():
    player = request.forms.get('player')
    if player == '1':
        print 'player1'
        return json.dumps(board2.getGoalParticle())
    print 'player2'
    return json.dumps(board1.getGoalParticle())

@route('/send_question', method='POST')
def send_question():
    global question_id
    player = request.forms.get('player')
    qid = request.forms.get('question')
    question_id = int(qid)
    print("PLAYER %s SENT QUESTION %s"%(player, qid))
    return ""

@route('/get_question', method='POST')
def get_question():
    global question_id, the_end
    if(request.forms.get('player') == "1"):
        if the_end == 0:
            return "win"
        elif the_end == 1:
            return "lose"
    else:
        if the_end == 1:
            return "win"
        elif the_end == 0:
            return "lose"

    if(question_id == -1):
        return "NONE"

    if(request.forms.get('player') == "1"):
        return board1.getQuestion(question_id)
    return board2.getQuestion(question_id)

@route('/send_answer', method='POST')
def send_answer():
    global answered, answer, question_id
    player = request.forms.get('player')
    answer = int(request.forms.get('answer'))
    print "Answer", answer
    if(player == '1'):
        answered = True
        ans = board2.checkAnswer(question_id, answer)
        ret = "CORRECT ANSWER" if (ans is True) else "WRONG ANSWER"
        answer = answer if (ans is True) else ans
        comment = board2.getExplanation(question_id)
        return json.dumps({'answer':ret, 'comment': comment})
    answered = True
    ans = board1.checkAnswer(question_id, answer)
    ret = "CORRECT ANSWER" if (ans is True) else "WRONG ANSWER"
    answer = answer if (ans is True) else ans
    comment = board1.getExplanation(question_id)

    print "ans", ans
    print "ret", ret
    print "comment", comment

    return json.dumps({'answer':ret, 'comment': comment})

@route('/get_answer', method='POST')
def get_answer():
    global answered, answer
    player = request.forms.get('player')
    print(answered, answer)
    if(not answered):
        print ("NONE")
        return "NONE"
    answered = False
    if(player == '1'):
        ret = "YES" if (answer == 1) else 'NO' if (answer == 0) else 'NO ANSWER'
        print (ret)
        return ret
    ret = 'YES' if (answer == 1) else 'NO' if (answer == 0) else 'NO ANSWER'
    print (ret)
    return ret

@route('/facedown', method='POST')
def send_static():
    player = request.forms.get('player')
    if player == '1':
        print "board1"
        return json.dumps(board1.getCardsFacedDown())
    print "board2"
    return json.dumps(board2.getCardsFacedDown())

@route('/verify', method='POST')
def send_static():
    global question_id
    player = request.forms.get('player')
    array = [int(x) for x in json.loads(request.forms.get('array'))]

    print array

    if player == '1':
        print "board1"
        res = board1.checkFaceDown(question_id, array)
        if res[1]==0 and res[2]==0:
            question_id = -1
        print board1.getCardsFacedDown()
        return json.dumps(res)
    print "board2"
    res = board2.checkFaceDown(question_id, array)
    if res[1]==0 and res[2]==0:
        question_id = -1

    print board2.getCardsFacedDown()
    return json.dumps(res)

@route('/solve', method='POST')
def send_static():
    player = request.forms.get('player')
    final_answer = int(request.forms.get('final_answer'))
    global the_end
    if player == '1':
        print "board1"
        res = board1.checkSolution(final_answer)
        if res == True:
            the_end = 0
            return "ok"
        the_end = 1
        return "wrong"
    print "board2"
    res= board2.checkSolution(final_answer)
    if res == True:
        the_end = 1
        return "ok"
    the_end = 0
    return "wrong"

@route('/idle', method='POST')
def send_static():
    player = request.forms.get('player')
    global wait1, wait2
    if player == '1':
        print "board1"
        if wait1 == True:
            wait2 = False
            return json.dumps('1')
        wait1=True
        return json.dumps('0')
    print "board2"
    if wait2 == True:
        wait1 = False
        return json.dumps('1')
    wait2=True
    return json.dumps('0')

run(host='127.0.0.1', port=8000, debug=True)
