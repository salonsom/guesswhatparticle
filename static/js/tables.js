
var prev;
var state;


$(document).ready(function() {
    $(".datepicker-modal").css("color", "#1a237e");
    $(".datepicker-date-display").css("background-color", "#1a237e");
    $(".dropdown-content>li>a").css("color", "#1a237e");
});

document.addEventListener('DOMContentLoaded', function() {
    
    $('.datepicker').datepicker();
    $('.timepicker').timepicker();
    check_state();
    
    
    $("#change_state").on("click", function(){
         
         if (state == 0){
            $.ajax({
                url: 'start_measurements',
                success: function(results) {
                console.log(results);
                if (results == "SUCCESS"){
                    state = 1 - state;
                    set_state(state); 
                    console.log("STARTED")
                    }
                },
                cache: false
            });
         }
         else{
            $.ajax({
                url: 'stop_measurements',
                success: function(results) {
                console.log(results);
                if (results == "SUCCESS"){
                    state = 1 - state;
                    set_state(state); 
                    console.log("STOPED");
                    }
                },
                cache: false
            });
         }

    }); 

});

function check_state(){
    $.ajax({
        url: 'is_running',
        success: function(results) {
            if (results == "true"){
                state = 1; 
                set_state(state);
                
            }
            else{
                state = 0;
                set_state(state);
                
            }
        },
        cache: false
    });
}

function set_state(state){
    if (state == 1){
        $("#change_state").css('background-color', '#f44336'); //RED
        $("#change_state").children("i").text("stop");
        $("#change_state").children("span").text("Stop Measurements");
   }
    else{
        $("#change_state").css('background-color', '#26a69a'); //GREEN
        $("#change_state").children("i").text("play_arrow");
        $("#change_state").children("span").text("Start Measurements");

    }    
}
