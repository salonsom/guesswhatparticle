
var chart; // global
var chart1;
var chart2;
var prev;
var state;
var state_live_button = 1;
function requestData() {
    var datefrom = ""+prev.getFullYear()+"-"+(prev.getMonth()+1)+"-"+prev.getDate();
    var timefrom = ""+prev.getHours()+":"+prev.getMinutes()+":"+prev.getSeconds(); 
    setTimeout(requestData, 5000);
    var data = {
        datefrom: datefrom,
        timefrom: timefrom
        };
    $.ajax({
        type: 'POST',
        url: 'querydata',
        data: data,
        success: function(results) {
            if(results != ""){
            var series = chart1.series[0],
            shift = series.data.length > 20; // shift if the series is longer than 20

            point = JSON.parse(results);;
            
            for(var it = 0; it < point.length; it ++){
            chart1.series[0].addPoint([new Date(point[it].date).getTime(), point[it].value], true, shift);
            }
            prev = new Date(point[point.length - 1].date);
            }
             
  
        },
        cache: false
    });
}


    $(".datepicker-modal").css("color", "#1a237e");
    $(".datepicker-date-display").css("background-color", "#1a237e");
    $(".dropdown-content>li>a").css("color", "#1a237e");
});

document.addEventListener('DOMContentLoaded', function() {
    $('#datefrom').datepicker({
        format: 'yyyy-mm-dd'
    });
    $('#timefrom').timepicker({
        twelveHour: false,
        defaultTime: '00:00'
    });
    $('#dateto').datepicker({
        format: 'yyyy-mm-dd'
    })
    $('#timeto').timepicker({
        twelveHour: false,
        defaultTime: '23:59'
    });
    check_state();
    
    $("#live-button").on("click", function(){
            state_live_button = 1 - state_live_button;
            set_state2(state_live_button);

    });
    $("#live-button").css('background-color', '#e53935'); //RED
    $("#live-button").css('color', '#FFFFFF'); //BLACK
    $("#live-button").css('border-color', '#FFFFFF'); //BLACK
    $("#live-button").children("i").text("fiber_manual_record");
    $("#live-button").children("span").text("Live");

    $.ajax({
        type: 'POST',
        url: 'querydata',
        success: function(results) {
            if(results != ""){

            point = JSON.parse(results);;

            var data_array1 = new Array();

            for(var it = 0; it < point.length; it ++){
                date_entry = new Date(point[it].date);
                var tuple = [date_entry.getTime(), point[it].value];
                data_array1.push(tuple);
                }
            prev = new Date(point[point.length - 1].date);
            chart1 = Highcharts.stockChart('graph1', {
                    chart: {
                        zoomType: 'xy',
                        pinchType: 'xy'
                    },
                    rangeSelector: {
                        selected: 1
                    },

                    title: {
                        text: 'Delay between insertions'
                    },

                    series: [{
                        name: 'Delay Between Insertions',
                        data: data_array1,
                        type: 'areaspline',
                        threshold: null,
                        tooltip: {
                            valueDecimals: 2
                        },
                        fillColor: {
                            linearGradient: {
                                x1: 0,
                                y1: 0,
                                x2: 0,
                                y2: 1
                            },
                            stops: [
                                [0, Highcharts.getOptions().colors[0]],
                                [1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
                            ]
                        }
                    }]
                });//END CHART 
                requestData();

            }//END IF
  
        },
        cache: false
    });
    
    $("#change_state").on("click", function(){
         
         if (state == 0){
            $.ajax({
                url: 'start_measurements',
                success: function(results) {
                console.log(results);
                if (results == "SUCCESS"){
                    state = 1 - state;
                    set_state(state); 
                    console.log("STARTED")
                    }
                },
                cache: false
            });
         }
         else{
            $.ajax({
                url: 'stop_measurements',
                success: function(results) {
                console.log(results);
                if (results == "SUCCESS"){
                    state = 1 - state;
                    set_state(state); 
                    console.log("STOPED");
                    }
                },
                cache: false
            });
         }

    });

    $("#send_form").on("click", function(){     
        console.log($("#form_send_data").serialize());
        $.ajax({
                type: 'POST',
                url: 'querydata',
                data: $("#form_send_data").serialize(),
                success: function(results) {
                    //console.log(results);
                    point = JSON.parse(results);;

                    var data_array1 = new Array();

                    for(var it = 0; it < point.length; it ++){
                        date_entry = new Date(point[it].date);
                        var tuple = [date_entry.getTime(), point[it].value];
                        data_array1.push(tuple);
                    }

                    chart1.series[0].data = data_array1;
                    console.log("CHANGED DATA ARRAY")
                },
                cache: false
            });
    });


});

function check_state(){
    $.ajax({
        url: 'is_running',
        success: function(results) {
            if (results == "true"){
                state = 1; 
                set_state(state);
                
            }
            else{
                state = 0;
                set_state(state);
                
            }
        },
        cache: false
    });
}

function set_state(state){
    if (state == 1){
        $("#change_state").css('background-color', '#f44336'); //RED
        $("#change_state").children("i").text("stop");
        $("#change_state").children("span").text("Stop Measurements");
   }
    else{
        $("#change_state").css('background-color', '#26a69a'); //GREEN
        $("#change_state").children("i").text("play_arrow");
        $("#change_state").children("span").text("Start Measurements");

    }    
}

function set_state2(state){
    if (state == 1){
        $("#live-button").css('background-color', '#e53935'); //RED
        $("#live-button").css('color', '#FFFFFF'); //BLACK
        $("#live-button").css('border-color', '#FFFFFF'); //BLACK
        $("#live-button").children("i").text("fiber_manual_record");
        $("#live-button").children("span").text("Live");
        $("#date_interval_form").slideToggle();
   }
    else{
        $("#live-button").css('background-color', '#43a047'); //GREEN
        $("#live-button").css('color', '#ffffff'); //BLACK
        $("#live-button").css('border-color', '#ffffff'); //BLACK
        $("#live-button").children("i").text("refresh");
        $("#live-button").children("span").text("Replay");
        $("#date_interval_form").slideToggle();

    }    
}
/*$(function () { 
    var myChart = Highcharts.chart('graph2', {
        chart: {
            type: 'bar'
        },
        title: {
            text: 'Fruit Consumption'
        },
        xAxis: {
            categories: ['Apples', 'Bananas', 'Oranges']
        },
        yAxis: {
            title: {
                text: 'Fruit eaten'
            }
        },
        series: [{
            name: 'Jane',
            data: [1, 0, 4]
        }, {
            name: 'John',
            data: [5, 7, 3]
        }]
    });
});*/
