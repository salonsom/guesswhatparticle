var waiting = true;
var data_player = {
    player: 1
}

var selected_images_ids = new Array();
var last_question_proposed = undefined;
var last_question_answered = undefined;

function remove_image_id(id) {
    var index = selected_images_ids.indexOf(id);
    if (index > -1) {
        selected_images_ids.splice(index, 1);
    }
}

function add_image_id(id) {
    selected_images_ids.push(id);
}

function flush_selected_images(){
    selected_images_ids.length = 0;
}
/*$(document).ready(function() {
        wait_function();
        while(waiting){}
        waiting = true;
});*/
document.addEventListener('DOMContentLoaded', function () {
    //This is loading the board for each player

    $.ajax({
        type: 'POST',
        url: 'random_board',
        data: data_player,
        success: function (results) {
            console.log(results)
            var name = "#content"
            var name2 = "#modalo4"
            list = JSON.parse(results);
            var txt = "<br>";
            var txt2 = "<br>"
            count_s1 = 0;
            count_s2 = 0;
            for (var i = 0; i < list.length; ++i) {
                if ((count_s2 % 5) == 0) {
                    txt += "<div class=\"row\"> <div class=\"col s3\"></div>";
                    txt2 += "<div class=\"row\"> <div class=\"col s3\"></div>";
                }
                var id = list[i];
                txt += "<div class=\"col s1\">\n" +
                           "<img class=\"responsive-img center-align particleimage\" src=\"static/img/" + id + ".jpeg\" id=\"" + i + "\">\n" +
                    "</div>";
                txt2 += "<div class=\"col s1\" style=\"width:80px;\">\n" +
                            "<img class=\"responsive-img center-align particlesolutioin\" src=\"static/img/" + id + ".jpeg\" " +
                    "onClick=\"click_answer("+ i +")\" id=\"solve" + i + "\">\n" +
                    "</div>";
                if ((count_s2++ % 5) == 4) {
                    txt += "<div class=\"col s3\"></div>\n</div>";
                    txt2 += "<div class=\"col s3\"></div>\n</div>";
                }
            }

            $(name).append(txt);
            $(name2).append(txt2);

            $('.particleimage').click(function () {
                if ($(this).hasClass('selectedimage')) {
                    $(this).removeClass('selectedimage');
                    remove_image_id($(this).attr('id'));
                } else {
                    $(this).addClass('selectedimage'); // adds the class to the clicked image
                    add_image_id($(this).attr('id'));
                }
                console.log(selected_images_ids);
            });

        },
        cache: false
    });
    $('.modal').modal({
        dismissible: false
    });
    $.ajax({
        type: 'POST',
        url: 'questions',
        data: data_player,
        success: function (results) {
            console.log(results)
            var name = "#modalo"
            var txt = "";
            table = JSON.parse(results);
            for (var i = 0; i < table.length; ++i) {
                txt += "<li class=\"collection-item question\" id=\"qid" + i + "\"onClick=\"click_question(" + i + ")\">" + table[i] + "</li>";
            }

            $(name).append(txt);


            setTimeout(game1, 1000);

        },
        cache: false
    });

    $("#restarting").on("click", function () {
        $.ajax({
            type: 'POST',
            url: 'restart',
            data: data_player,
            success: function (results) {
                console.log("RESTART")
                var txt = "";
                $(name).append(txt);
                window.location = '/index'
                setTimeout(game1, 1000);
            },
            cache: false
        });
    });

    $("#restarting2").on("click", function () {
        $.ajax({
            type: 'POST',
            url: 'restart',
            data: data_player,
            success: function (results) {
                console.log("RESTART")
                var txt = "";
                $(name).append(txt);
                window.location = '/index'
                setTimeout(game1, 1000);
            },
            cache: false
        });
    });

    $("#restarting3").on("click", function () {
        $.ajax({
            type: 'POST',
            url: 'restart',
            data: data_player,
            success: function (results) {
                console.log("RESTART")
                var txt = "";
                $(name).append(txt);
                window.location = '/index'  
                setTimeout(game1, 1000);
            },
            cache: false
        });
    });

    $.ajax({
        type: 'POST',
        url: 'player_particle',
        data: data_player,
        success: function (results) {
            console.log(results)
            var name = "#particle-frame"
            var table = JSON.parse(results);
            var txt = "<img class=\"responsive-img center-align\" src=\"static/img/" + table + ".jpeg\" id=\"9\"\n" +
                "                         style=\"max-width: 50%; max-height: 100%\">";

            $(name).append(txt);


            setTimeout(game1, 1000);

        },
        cache: false
    });

     $("#yes").on("click", function () {
        var data_player = {
            player: 1,
            answer: 1
        }
        $.ajax({
            type: 'POST',
            url: 'send_answer',
            data: data_player,
            success: function (results) {
                //console.log("Call to function " + results + "\n");
                console.log(results);
                var ans = JSON.parse(results);
                M.Modal.getInstance($("#modal3")).close();
                M.Modal.getInstance($("#modal4")).open();
                $("#ans-correct-answer").text(ans.answer);
                $("#ans-question").text(last_question_answered);
                $("#ans-comment").text(ans.comment);

            },
            cache: false
        });
    });

        $("#no").on("click", function () {
        var data_player = {
            player: 1,
            answer: 0
        }
        $.ajax({
            type: 'POST',
            url: 'send_answer',
            data: data_player,
            success: function (results) {
                console.log(results);
                var ans = JSON.parse(results);
                M.Modal.getInstance($("#modal3")).close();
                M.Modal.getInstance($("#modal4")).open();
                $("#ans-correct-answer").text(ans.answer);
                $("#ans-question").text(last_question_answered);
                $("#ans-comment").text(ans.comment);


            },
            cache: false
        });
    });

    $("#noans").on("click", function () {
        var data_player = {
            player: 1,
            answer: 2
        }
        $.ajax({
            type: 'POST',
            url: 'send_answer',
            data: data_player,
            success: function (results) {
                console.log(results);
                var ans = JSON.parse(results);
                M.Modal.getInstance($("#modal3")).close();
                M.Modal.getInstance($("#modal4")).open();
                $("#ans-correct-answer").text(ans.answer);
                $("#ans-question").text(last_question_answered);
                $("#ans-comment").text(ans.comment);



            },
            cache: false
        });
    });

    $("#checker").on("click", function () {
        var myJsonString = JSON.stringify(selected_images_ids);
        var data_player = {
            player: 1,
            array: myJsonString
        }
        $.ajax({
            type: 'POST',
            url: 'verify',
            data: data_player,
            success: function (results) {
                var t = JSON.parse(results);

                M.toast({html: "CORRECT: " + t[0]});
                M.toast({html: "INCORRECT: " + t[1]});
                M.toast({html: "CHANGES: " + t[2]});
                if (t[1] == 0 && t[2] == 0) {
                    setTimeout(function () {
                        console.log("next state");

                        $('.selectedimage').addClass('fixedselection');
                        $('.selectedimage').removeClass('particleimage');
                        $('.selectedimage').removeClass('selectedimage');
                        $(".fixedselection").unbind("click");
                        flush_selected_images();
                        setTimeout(game3, 100);
                            console.log(selected_images_ids);
                    }, 1000);
                }
            },
            cache: false
        });
    });

    $("#answer_footer").on("click", function(){
        M.Modal.getInstance($("#modal4")).close();
        setTimeout(game1, 100);
    });

    $("#solving").on("click", function(){
        M.Modal.getInstance($("#modal6")).open();
    });


});

function click_question(id) {
    console.log("clicked question is: " + id);
    var el = "#qid" + id;
    console.log(el);
    last_question_proposed = $(el).text();
    console.log(last_question_proposed);
    var data_player = {
        player: 1,
        question: id
    }
    $.ajax({
        type: 'POST',
        url: 'send_question',
        data: data_player,
        success: function (results) {
            console.log("question correctly received")
            M.Modal.getInstance($("#modal1")).close();
            setTimeout(game2, 100);
        },
        cache: false
    });

}

function click_answer(id) {
    console.log("clicked question is: " + id);
    var el = "#solve" + id;
    console.log(el);
    var data_player = {
        player: 1,
        final_answer: id
    }
    $.ajax({
        type: 'POST',
        url: 'solve',
        data: data_player,
        success: function (results) {
            console.log(results);
            if(results == "ok"){
                console.log("Display you win");
                M.Modal.getInstance($("#modal8")).open();
            }
            else if (results == "wrong"){
                console.log("LOSES");
                M.Modal.getInstance($("#modal7")).open();
            }
        },
        cache: false
    });

}

function wait_function() {
    $.ajax({
        type: 'POST',
        url: 'idle',
        data: data_player,
        success: function (results) {
            //console.log("Call to function " + results + "\n");
            if (results == 0) {
                console.log("NO MORE IDLE");
                waiting = false;
            }
        },
        cache: false
    });
}

function get_question() {
    $.ajax({
        type: 'POST',
        url: 'get_question',
        data: data_player,
        success: function (results) {
            //console.log("Call to function " + results + "\n");
            console.log(results);
            if(results != "NONE"){

                if(results == "win"){
                    console.log("win");
                     M.Modal.getInstance($("#modal8")).open();
                }
                else if(results == "lose"){
                    console.log("lost");
                    M.Modal.getInstance($("#modal7")).open();
                }
                else {
                    question = results;
                    last_question_answered = question;
                    $("#question-to-answer").text(question);
                    M.Modal.getInstance($("#modal3")).open();
                }
                waiting = false;

            }

        },
        cache: false
    });
}


function get_answer() {
    $.ajax({
        type: 'POST',
        url: 'get_answer',
        data: data_player,
        success: function (results) {
            console.log(results);
            if (results != "NONE") {
                console.log(results)
                ans = last_question_proposed + " " + results;
                var name = "#modalo3";
                console.log(last_question_proposed);
                var txt = "<li class=\"collection-item question\">" + ans + "</li>";
                $(name).append(txt);
                waiting = false;
                M.Modal.getInstance($("#modal5")).open();
            }


        },
        cache: false
    });
}

function game1() {
    M.Modal.getInstance($("#modal1")).open();

}

function game2() {
    if (waiting) {
        get_answer();
        setTimeout(game2, 1000);
    }
    else {
        waiting = true;
    }
}

function game3(){
    if (waiting) {
        get_question();
        setTimeout(game3, 1000);
    }
    else {
        waiting = true;
    }
}




