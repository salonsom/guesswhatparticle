"""
This is the module that stores the backbone classes of the software.
"""

__version__ = '0.1'
__author__ = 'Saul Alonso-Monsalve'
__email__ = "saul.alonso.monsalve@cern.ch"

import random
from random import randint, shuffle
from datetime import datetime

random.seed(datetime.now())

class Particle(object):

    def __init__(self, name, answers, chosen_particle=False):
        'Initialization'
        self.name = name
        self.answers = answers

class Card(object):

    def __init__(self, particle_id, face_down=False):
        'Initialization'
        self.particle_id = particle_id
        self.face_down = face_down

class Board(object):

    def __init__(self, particles, questions, explanations, shuff=True):
        'Initialization'
        self.particles = particles
        self.questions = questions
        self.explanations = explanations

        # generate a random board by shuffling the cards
        id_list = range(len(self.particles))
        if shuff == True:
            shuffle(id_list)
        self.cards=[]
        for i in id_list:
            self.cards.append(Card(particle_id=i))

        self.setGoal(random=True) # not goal card yet

    def setMyGoal(self, particle_id):
        'Set a custom goal'
        for index in range(len(self.cards)):
            if self.cards[index].particle_id == particle_id:
                self.goal_index = index

    def getExplanation(self, question_id):
        'Return the explanation of a specific question from a particle'
        return self.explanations[self.getParticleID(self.goal_index)][question_id]

    def getGoalParticle(self):
        'Return the index of the goal particle'
        return self.getParticleID(self.goal_index)

    def getParticleName(self, particle_id):
        'Return the particle name from a particle ID'
        return self.particles[particle_id].name

    def getParticleAnswer(self, particle_id, question_id):
        'Return the answer of a question from for a particular particle'
        return self.particles[particle_id].answers[question_id]
 
    def getParticleID(self, index):
        'Return the particle ID from a card index'
        return self.cards[index].particle_id

    def getCardsIDs(self):
        'Return the particle IDs from the cards set'
        return [card.particle_id for card in self.cards]

    def getCardsFacedDown(self):
        'Return a list in where each position specifies whether a card is faced down'
        return [card.face_down for card in self.cards]

    def getQuestion(self, question_id):
        'Return the text of the question from a question ID'
        return self.questions[question_id]

    def getQuestions(self):
        'Return the questions'
        return self.questions

    def setGoal(self, goal_index=0, random=False):
        'Set the goal particle'
        if random == True:
            self.goal_index = randint(0, len(self.cards)-1)
        else:
            self.goal_index = goal_index

    def checkAnswer(self, question_id, candidate_answer):
        'Check the user answer'
        true_answer = self.getParticleAnswer(self.getParticleID(self.goal_index), question_id)
        if true_answer == candidate_answer:
            return True
        else:
            return true_answer

    def getCardIndexAnswers(self, question_id):
        'Get the list of indexes that correspond to the particles with the same answer to the question as the goal particle'
        true_answer = self.checkAnswer(question_id, -1)
        print "True answer:", true_answer
        res = []
        for index in range(len(self.cards)):
            if self.getParticleAnswer(self.getParticleID(index), question_id) != true_answer and self.cards[index].face_down == False:
                res.append(index)
        return res

    def checkFaceDown(self, question_id, index_list):
        'Check the number of cards that the user faced down correctly'

        true_indexes = self.getCardIndexAnswers(question_id)

        correct = list(set(index_list).intersection(true_indexes))
        incorrect = list(set(index_list) - set(correct))
        needed = list(set(true_indexes) - set(correct))

        print self.getParticleID(self.goal_index)
        print index_list
        print true_indexes
        print correct
        print incorrect
        print needed

        if len(incorrect) == 0 and len(needed) == 0:
            self.checkQuestion(question_id)

        return len(correct), len(incorrect), len(needed)

    def checkQuestion(self, question_id):
        'Face down cards that dont share the same feature'
        print self.getQuestion(question_id)

        goal_particle_id = self.getParticleID(self.goal_index)
        goal_particle_answer = self.getParticleAnswer(goal_particle_id, question_id)

        for card in self.cards:
            particle_id = card.particle_id
            if self.getParticleAnswer(particle_id, question_id) != goal_particle_answer:
                card.face_down = True
    
    def checkSolution(self, candidate_index):
        'Check whether the candidate matches the goal particle'
        if candidate_index == self.goal_index:
            return True
        return False

    def printBoard(self):
        'Print the board'
        print '-------------------------------------------------------------------------'
        if self.goal_index is not None:
            print 'Goal particle:', self.getParticleName(self.getParticleID(self.goal_index))
            print '----'
        for card in self.cards:
            print self.getParticleName(card.particle_id), ':', card.face_down
        print '-------------------------------------------------------------------------'
